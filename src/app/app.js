angular.module('marvel', ['marvel.welcome', 'marvel.quiz', 'marvel.result', 'marvel.graphics'])
.controller('homeCtrl', ['$scope', function ($scope) {
	$scope.welm = true;
	$scope.$parent.asks = false;
	$scope.$parent.results = false;

	$scope.$parent.starts = {
		nombres: [],
		promedios: [],
		allprom: [[0, 0, 0, 0, 0]]
	};
}]);