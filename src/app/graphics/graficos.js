angular.module('marvel.graphics', [])
.directive("grpersonalidades", function() {
	return {
		restrict: 'E',
		controller: 'personalidadesCtrl',
		templateUrl: 'src/app/graphics/grpersonalidades.html',
	};
})
.directive("grgeneral", function() {
	return {
		restrict: 'E',
		controller: 'generalCtrl',
		templateUrl: 'src/app/graphics/grgeneral.html',
	};
})
.controller('personalidadesCtrl', ['$scope', function ($scope) {
	var arr_prom = [];
	$scope.$parent.starts.allprom = [[0, 0, 0, 0, 0]];

	for (var k = 0; k <= ($scope.$parent.starts.promedios).length - 1; k++) {
		arr_prom.push(k+1);
		for (var ii = 0; ii <= ($scope.$parent.starts.promedios[k]).length - 1; ii++) {
			arr_prom.push($scope.$parent.starts.promedios[k][ii]);
		}
		$scope.$parent.starts.allprom.push(arr_prom);
		arr_prom = [];
	}
	
	arr_prom = [];

	google.charts.load('current', {'packages':['line']});
	google.charts.setOnLoadCallback(drawChart);

	function drawChart() {

		var data = new google.visualization.DataTable();
		data.addColumn('number', '(%) PERSONALIDAD POR CADA N° INTENTOS');
		for (var i = 0; i <= ($scope.$parent.starts.nombres).length - 1; i++) {
			data.addColumn('number', $scope.$parent.starts.nombres[i]);
		}
		data.addRows($scope.$parent.starts.allprom);

		var options = {
			axes: {
				x: {
					0: {side: 'top'}
				}
			}
		};

		var chartp = new google.charts.Line(document.getElementById('linechart_material'));

		chartp.draw(data, options);
	}
}])
.controller('generalCtrl', ['$scope', function ($scope) {
	var graph = [['Heroes', 'Promedio']];
		
	angular.forEach($scope.$parent.heroes, function(obj, x) {
		graph.push([obj.nombre, obj.aciertos]);
	});
	google.charts.load("current", {packages:["corechart"]});
	google.charts.setOnLoadCallback(drawChartg);

	function drawChartg() {
		var data = google.visualization.arrayToDataTable(graph);

		var options = {
			title: '(%) de Personalidad',
			pieHole: 0.4,
		};

		var chart = new google.visualization.PieChart(document.getElementById('graphgeneral'));
		chart.draw(data);
	}
}]);
