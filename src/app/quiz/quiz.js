angular.module('marvel.quiz', [])
.directive("quiz", function() {
	return {
		restrict: 'E',
		controller: 'quizCtrl',
		templateUrl: 'src/app/quiz/quiz.html',
	};
})
.controller('quizCtrl', ['$scope', function ($scope) {
	$scope.counter = 1;
	$scope.actualp = 0;
	$scope.stop = true;
	$scope.eval = false;
	$scope.response = 1;


	// ------------------>>>>  User
	$scope.resultado = {
		respuestas: [],
	};

	// ------------------>>>>  MARVEL
	$scope.$parent.heroes = [
		{
			nombre: 'Hulk',
			icon: 'assets/img/heroes/hulk/icon-hulk.png',
			img: ['assets/img/heroes/hulk/hulk1.png', 'assets/img/heroes/hulk/hulk2.png'],
			// esheroe: [4, 2, 0, 4, 4, 2, 1, 1, 5, 5, 4, 0, 4, 3, 3]
			esheroe: [0, 2, 0, 4, 1, 2, 1, 1, 5, 5, 1, 0, 1, 1, 1, 0, 0],
			promedio: 0,
			aciertos: 0
		},
		{
			nombre: 'Tony Stark (Iron Man)',
			icon: 'assets/img/heroes/iron-man/icon-ironman.png',
			img: ['assets/img/heroes/iron-man/iron-man1.png', 'assets/img/heroes/iron-man/iron-man2.png'],
			// esheroe: [1, 4, 1, 2, 1, 1, 2, 3, 2, 1, 2, 2, 3, 0, 5]
			esheroe: [0, 4, 1, 2, 1, 1, 1, 3, 2, 1, 0, 2, 1, 0, 0, 0, 0],
			promedio: 0,
			aciertos: 0
		},
		{
			nombre: 'Loki',
			icon: 'assets/img/heroes/loki/icon-loki.jpg',
			img: ['assets/img/heroes/loki/loki1.png', 'assets/img/heroes/loki/loki2.png'],
			// esheroe: [1, 3, 3, 3, 5, 3, 3, 2, 4, 3, 0, 0, 1, 0, 3]
			esheroe: [1, 3, 3, 3, 0, 3, 0, 2, 1, 3, 0, 0, 1, 0, 0, 1, 0],
			promedio: 0,
			aciertos: 0
		},
		{
			nombre: 'Spiderman',
			icon: 'assets/img/heroes/spiderman/icon-spiderman.jpg',
			img: ['assets/img/heroes/spiderman/spiderman.png','assets/img/heroes/spiderman/spiderman2.png'],
			// esheroe: [2, 1, 2, 1, 3, 3, 2, 4, 2, 2, 1, 3, 2, 1, 5]
			esheroe: [0, 1, 2, 1, 0, 3, 0, 4, 4, 2, 1, 3, 0, 1, 0, 0, 0],
			promedio: 0,
			aciertos: 0
		}
	];

	
	// ------------------>>>>  Questionario
	$scope.preguntas = [
		{//--> 0 --> 14
			nombre : 'Tiendes a ser una persona:',
			opcs : {
				'a' : 'Reservada',
				'b' : 'Intimidante',
				'c' : 'furiosa',
				'd' : 'Alegre',
				'e' : false,
			},
			values: [[1, 1], [1, 0], [0, 1], [0, 0]],
			pos: [0,14]
		},
		{//--> 1
			nombre : 'Me encuentro en medio de una pelea con personas de la mafia, yo ...',
			opcs : {
				'a' : 'Me retiro.',
				'b' : 'Los golpeo a todos.',
				'c' : 'Observo la pelea.',
				'd' : 'Evito que peleen.',
				'e' : false,
			},
			values: [1, 2, 3, 4],
			pos: [1]
		},
		{//--> 2
			nombre : 'Queda una semana para entregar un proyecto, en el cuál se decidirá el futuro de la empresa,'+
			' pero no hay nadie a cargo del proyecto y todo el equipo no hizo nada. ¿Que harías?',
			opcs : {
				'a' : 'Tomo el mando de la situación y organizo una investigación profunda para obtener resultados.',
				'b' : 'Espero a que alguien tome el mando.',
				'c' : 'Esperaria el momento en que mis habilidades sean útiles.',
				'd' : false,
				'e' : false,
			},
			values: [1, 2, 3],
			pos: [2]
		},
		{//--> 3
			nombre : 'Cuando hay que terminar un trabajo urgente:',
			opcs : {
				'a' : 'Intento que alguien me ayude para terminar a tiempo.',
				'b' : 'Realizo horas extras hasta terminarlo.',
				'c' : 'Hago que los demás me ayuden a terminarlo.',
				'd' : 'No me intereza si lo termino o no.',
				'e' : false,
			},
			values: [1, 2, 3, 4],
			pos: [3]
		},
		{//--> 4 --> 10 --> 15
			nombre : '¿El amor para ti es?',
			opcs : {
				'a' : 'Libertad.',
				'b' : 'Procreación.',
				'c' : 'Seguridad.',
				'd' : 'No lo entiendo.',
				'e' : 'Interrogante.',
			},
			values: [[1, 1, 1], [1, 0, 0], [0, 1, 1], [0, 0, 0], [0, 0, 1]],
			pos: [4, 10, 15]
		},
		{//--> 5 --> 13
			nombre : 'Es más difícil para ti:',
			opcs : {
				'a' : 'Utilizar a otras personas.',
				'b' : 'Comunicarte con otras personas.',
				'c' : 'Identificarte con otros.',
				'd' : false,
				'e' : false,
			},
			values: [[1, 1], [2, 0], [3, 1]],
			pos: [5, 13]
		},
		{//--> 6
			nombre : 'Tus emociones te controlan más de lo que tú las controlas.',
			opcs : {
				'a' : 'Totalmente de acuerdo',
				'b' : 'De acuerdo.',
				'c' : 'Ni a favor ni en contra.',
				'd' : 'En desacuerdo.',
				'e' : 'Totalmente en desacuerdo.',
			},
			values: [1, 2, 3, 4, 5],
			pos: [6]
		},
		{//--> 7 --> 11
			nombre : 'En una conversación:',
			opcs : {
				'a' : 'Suelo escuchar y no hablar.',
				'b' : 'Siempre me escuchan lo que digo.',
				'c' : 'Expongo mis ideas y opiniones.',
				'd' : 'Prefiero escuchar las opiniones de los demás para luego hablar yo.',
				'e' : false,
			},
			values: [1, 2, 3, 4],
			pos: [7, 11]
		},
		{//--> 8 --> 12 --> 16
			nombre : '¿Crees que un amigo es?',
			opcs : {
				'a' : 'Un cómplice.',
				'b' : 'Un Apoyo.',
				'c' : 'Alguien que me interesa.',
				'd' : 'Un objeto si valor.',
				'e' : 'Un juguete.',
			},
			values: [[1, 1, 1], [2, 1, 0], [3, 0, 1], [4, 0, 0], [5, 1, 0]],
			pos: [8, 12, 16]
		},
		{//--> 9
			nombre : 'Me gusta un estilo de vida relajado.',
			opcs : {
				'a' : 'Totalmente de acuerdo',
				'b' : 'De acuerdo.',
				'c' : 'Ni a favor ni en contra.',
				'd' : 'En desacuerdo.',
				'e' : 'Totalmente en desacuerdo.',
			},
			values: [1, 2, 3, 4, 5],
			pos: [9]
		},
	];
	$scope.back = function () {
		$scope.stop = true;
		$scope.eval = false;
		$scope.actualp--;
	};
	$scope.next = function () {
		$scope.stop = true;
		$scope.actualp++;
	};
	$scope.values = function (e, r) {
		if($scope.actualp >= $scope.preguntas.length-1){
			$scope.stop = true;
			$scope.eval = true;
		}else {
			$scope.stop = false;
		}
		for (var i = 0; i <= (r.pos).length - 1; i++) {
			if((r.values[e]).length > 1){
				$scope.resultado.respuestas[r.pos[i]] = r.values[e][i];
			}else {
				$scope.resultado.respuestas[r.pos[i]] = r.values[e];
			}
		}
	};

	
	$scope.evaluar = function () {
		var ar_prom = [];
		var ar_nombres = [];
		
		angular.forEach($scope.heroes, function(obj, x) {
			for (var i = 0; i <= (obj.esheroe).length - 1; i++) {
				if($scope.resultado.respuestas[i] == obj.esheroe[i]) {
					obj.aciertos++;
				}
			};
			obj.promedio = parseFloat(((obj.aciertos / (obj.esheroe).length) * 100).toFixed(4));
			ar_prom.push(obj.promedio);
			ar_nombres.push(obj.nombre);
		});

		$scope.$parent.starts.nombres = ar_nombres;
		$scope.$parent.starts.promedios.push(ar_prom);

		$scope.$parent.asks = false;
		$scope.$parent.results = true;
	};

}])
.filter('startFrom', function() {
	return function(input, start) {
		start = +start;
		return input.slice(start);
	}
});
