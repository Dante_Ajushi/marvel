angular.module('marvel.welcome', [])
.directive("welcome", function() {
	return {
		restrict: 'E',
		controller: 'welcomeCtrl',
		templateUrl: 'src/app/welcome/welcome.html',
	};
})
.controller('welcomeCtrl', ['$scope', function($scope){
	$scope.start = function () {
		$('.welcome').hide(500, function () {
		});
			$scope.$parent.asks = true;
			$scope.$parent.welm = false;
	};
}]);