angular.module('marvel.result', [])
.directive("result", function() {
	return {
		restrict: 'E',
		controller: 'resultCtrl',
		templateUrl: 'src/app/result/result.html',
	};
})
.controller('resultCtrl', ['$scope', 'orderByFilter', function ($scope, orderBy) {
	$scope.$parent.heroes = orderBy($scope.$parent.heroes, 'promedio', true);
	$scope.$parent.grmain = true;
	$scope.$parent.general = $scope.$parent.personalidades = false;

	$scope.esheroe = $scope.$parent.heroes[0];
	$scope.esheroe.img = $scope.$parent.heroes[0].img[Math.floor((Math.random() * ($scope.$parent.heroes[0].img).length))];

	$scope.$parent.graphics = function(r) {
		function close() {
			$scope.$parent.results = false;
			return true;
		}
		$scope.$parent.grmain = (r==3)?true:false;
		$scope.$parent.personalidades = (r==1)?true:false;
		$scope.$parent.general = (r==2)?true:false;
		$scope.$parent.asks = (r==4)?close():false;
	};
}]);
